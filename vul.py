from netmiko import ConnectHandler
from getpass import getpass

def auth():#Function to take in the credentials
    u = input("\nUsername: ")
    p = getpass("Password: ") #Works with terminal not on IDLE.

    return u,p

def lists():#Function that opens the cis document & commands file and returns a list from each.

    inf = open("cis","r")
    a = inf.read()
    dL = a.rstrip().split("\n")
    
    mL = []

    for i in dL:
        mL.append(i.split())

    return mL

def ssh(ip,u,p): #Function for creation of ssh channel
    dev = {'device_type':"cisco_ios",'ip':ip,'username':u,'password':p}

    try:
        nC =ConnectHandler(**dev)

    except:
        nC = "XX"

    return nC

def main():
    u,p = auth()
    mL  = lists()
    a = open("ff.txt","w")
    a.close()

    b = open("ff.txt","a")
    
    try:
        for i in mL:
            hn = i[1]
            print("\nCI: ",hn)
            if "fw" in hn:
                print("SKIPPING - Firewall")

            else:
                nC = ssh(i[0],u,p)
                if nC == "XX":
                    print("DEVICE UNREACHED or WRONG CREDENTIALS")

                else:
                    o = nC.send_command("show run")
                    xx = crypto(o) #Funtion to look for crypto PKI trustpoint configuration.

                    if xx == "1":
                        services(b,hn,o)
                    else:
                        print("NO RISK : No trustpoint enrollment")
                    
        print("======THE END=========")
        b.close()

    except KeyboardInterrupt:
        print("Script stopped by user")


def crypto(o):
    if "enrollment" in o:
        xx = "1"

    else:
        xx = "2"
	
    return xx

def services(b,hn,o):
    b.write("\n==========================\n")
    b.write(hn)
    b.write("\nSelf-assigned certicate present and enrolled\n")
    if "ip http secure-trustpoint" in o:
        b.write("self-signed certificate is tied to the device’s web server ")
	
    elif "crypto signaling" in o:
        b.write("Used in SIP over TLS")

    elif "secure-signaling trustpoint" in o:
        b.write("Cisco Unified CME with encrypted signaling enabled")

    elif "credentials" in o:
        b.write("Cisco Unified SRST with encrypted signaling enabled")

    elif "dspfarm profile" in o:
        b.write("Cisco IOS dspfarm resources enabled")

    elif "stcapp security trustpoint" in o:
        b.write("STCAPP ports configured with encrypted signaling")

    elif "ssl trustpoint" in o:
        b.write("SSLVPN configured")

    elif "crypto isakmp" in o:
        b.write("ISAKMP and IKEv2")

    elif "crypto ikev2" in o:
        b.write("ISAKMP and IKEv2")

    elif "trustpoint sign" in o:
        b.write("SSH Server")

    elif "restconf" in o:
        b.write("RESTCONF in device")

    else:
        b.write("NO RISK : Self-signed certificate NOT assigned to a device feauture")

main()



